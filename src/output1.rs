PLY: PARSE DEBUG START

State  : 0
Stack  : . LexToken(FN,'fn',1,0)
Action : Shift and goto state 3

State  : 3
Stack  : FN . LexToken(MAIN_FUN,'main',1,3)
Action : Shift and goto state 5

State  : 5
Stack  : FN MAIN_FUN . LexToken(OPEN_PARANTHESIS,'(',1,7)
Action : Shift and goto state 6

State  : 6
Stack  : FN MAIN_FUN OPEN_PARANTHESIS . LexToken(CLOSE_PARANTHESIS,')',1,8)
Action : Shift and goto state 7

State  : 7
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS . LexToken(BEGIN_BLOCK,'{',1,9)
Action : Shift and goto state 8

State  : 8
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK . LexToken(PRINT,'println!',1,12)
Action : Shift and goto state 12

State  : 12
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK PRINT . LexToken(OPEN_PARANTHESIS,'(',1,20)
Action : Shift and goto state 33

State  : 33
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK PRINT OPEN_PARANTHESIS . LexToken(SENTENCE,'"Hello World!"',1,21)
Action : Shift and goto state 49

State  : 49
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE . LexToken(CLOSE_PARANTHESIS,')',1,35)
Action : Shift and goto state 64

State  : 64
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS . LexToken(SEMICOLON,';',1,36)
Action : Shift and goto state 103

State  : 103
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON . LexToken(END_BLOCK,'}',1,38)
Action : Reduce rule [non_block_stmt -> PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON] with ['println!','(',<str @ 0xb7160598>,')',';'] and goto state 17
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt . LexToken(END_BLOCK,'}',1,38)
Action : Reduce rule [stmts -> non_block_stmt] with [None] and goto state 4
Result : <NoneType @ 0x833a224> (None)

State  : 14
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,38)
Action : Shift and goto state 34

State  : 34
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK . $end
Action : Reduce rule [main_fun -> FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK] with ['fn','main','(',')','{',None,'}'] and goto state 3
Result : <NoneType @ 0x833a224> (None)

State  : 2
Stack  : main_fun . $end
Action : Reduce rule [ProgramFile -> main_fun] with [None] and goto state 2
Result : <NoneType @ 0x833a224> (None)

State  : 4
Stack  : ProgramFile . $end
Action : Reduce rule [compilation_unit -> ProgramFile] with [None] and goto state 1
Result : <NoneType @ 0x833a224> (None)

State  : 1
Stack  : compilation_unit . $end
Done   : Returning <NoneType @ 0x833a224> (None)
PLY: PARSE DEBUG END
