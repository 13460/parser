PLY: PARSE DEBUG START

State  : 0
Stack  : . LexToken(FN,'fn',1,0)
Action : Shift and goto state 3

State  : 3
Stack  : FN . LexToken(MAIN_FUN,'main',1,3)
Action : Shift and goto state 5

State  : 5
Stack  : FN MAIN_FUN . LexToken(OPEN_PARANTHESIS,'(',1,7)
Action : Shift and goto state 6

State  : 6
Stack  : FN MAIN_FUN OPEN_PARANTHESIS . LexToken(CLOSE_PARANTHESIS,')',1,8)
Action : Shift and goto state 7

State  : 7
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS . LexToken(BEGIN_BLOCK,'{',1,9)
Action : Shift and goto state 8

State  : 8
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK . LexToken(LET,'let',1,12)
Action : Shift and goto state 27

State  : 27
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET . LexToken(ident,'x',1,16)
Action : Shift and goto state 42

State  : 42
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident . LexToken(ASSIGNMENT_OP,'=',1,18)
Action : Shift and goto state 56

State  : 56
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP . LexToken(LIT_INTEGER,5,1,20)
Action : Shift and goto state 95

State  : 95
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER . LexToken(SEMICOLON,';',1,21)
Action : Shift and goto state 118

State  : 118
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON . LexToken(FOR,'for',1,26)
Action : Reduce rule [non_block_stmt -> LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON] with ['let','x','=',5,';'] and goto state 9
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt . LexToken(FOR,'for',1,26)
Action : Shift and goto state 19

State  : 19
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR . LexToken(ident,'i',1,30)
Action : Shift and goto state 36

State  : 36
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident . LexToken(IN,'in',1,32)
Action : Shift and goto state 50

State  : 50
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN . LexToken(LIT_INTEGER,1,1,35)
Action : Shift and goto state 65

State  : 65
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER . LexToken(D_DOT,'..',1,36)
Action : Shift and goto state 104

State  : 104
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT . LexToken(LIT_INTEGER,10,1,38)
Action : Shift and goto state 122

State  : 122
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER . LexToken(BEGIN_BLOCK,'{',1,40)
Action : Shift and goto state 134

State  : 134
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK . LexToken(PRINT,'println!',1,44)
Action : Shift and goto state 12

State  : 12
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK PRINT . LexToken(OPEN_PARANTHESIS,'(',1,52)
Action : Shift and goto state 33

State  : 33
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK PRINT OPEN_PARANTHESIS . LexToken(SENTENCE,'"Hello, World"',1,53)
Action : Shift and goto state 49

State  : 49
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE . LexToken(CLOSE_PARANTHESIS,')',1,67)
Action : Shift and goto state 64

State  : 64
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS . LexToken(SEMICOLON,';',1,68)
Action : Shift and goto state 103

State  : 103
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON . LexToken(END_BLOCK,'}',1,71)
Action : Reduce rule [non_block_stmt -> PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON] with ['println!','(',<str @ 0xb7224598>,')',';'] and goto state 17
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK non_block_stmt . LexToken(END_BLOCK,'}',1,71)
Action : Reduce rule [stmts -> non_block_stmt] with [None] and goto state 4
Result : <NoneType @ 0x833a224> (None)

State  : 138
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,71)
Action : Shift and goto state 141

State  : 141
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK stmts END_BLOCK . LexToken(END_BLOCK,'}',1,73)
Action : Reduce rule [for_block -> FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK stmts END_BLOCK] with ['for','i','in',1,'..',10,'{',None,'}'] and goto state 48
Result : <NoneType @ 0x833a224> (None)

State  : 9
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt for_block . LexToken(END_BLOCK,'}',1,73)
Action : Reduce rule [block_stmt -> for_block] with [None] and goto state 37
Result : <NoneType @ 0x833a224> (None)

State  : 17
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt block_stmt . LexToken(END_BLOCK,'}',1,73)
Action : Reduce rule [stmts -> block_stmt] with [None] and goto state 6
Result : <NoneType @ 0x833a224> (None)

State  : 40
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt stmts . LexToken(END_BLOCK,'}',1,73)
Action : Reduce rule [stmts -> non_block_stmt stmts] with [None,None] and goto state 5
Result : <NoneType @ 0x833a224> (None)

State  : 14
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,73)
Action : Shift and goto state 34

State  : 34
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK . $end
Action : Reduce rule [main_fun -> FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK] with ['fn','main','(',')','{',None,'}'] and goto state 3
Result : <NoneType @ 0x833a224> (None)

State  : 2
Stack  : main_fun . $end
Action : Reduce rule [ProgramFile -> main_fun] with [None] and goto state 2
Result : <NoneType @ 0x833a224> (None)

State  : 4
Stack  : ProgramFile . $end
Action : Reduce rule [compilation_unit -> ProgramFile] with [None] and goto state 1
Result : <NoneType @ 0x833a224> (None)

State  : 1
Stack  : compilation_unit . $end
Done   : Returning <NoneType @ 0x833a224> (None)
PLY: PARSE DEBUG END
