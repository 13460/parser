PLY: PARSE DEBUG START

State  : 0
Stack  : . LexToken(FN,'fn',1,0)
Action : Shift and goto state 3

State  : 3
Stack  : FN . LexToken(MAIN_FUN,'main',1,3)
Action : Shift and goto state 5

State  : 5
Stack  : FN MAIN_FUN . LexToken(OPEN_PARANTHESIS,'(',1,7)
Action : Shift and goto state 6

State  : 6
Stack  : FN MAIN_FUN OPEN_PARANTHESIS . LexToken(CLOSE_PARANTHESIS,')',1,8)
Action : Shift and goto state 7

State  : 7
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS . LexToken(BEGIN_BLOCK,'{',1,9)
Action : Shift and goto state 8

State  : 8
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK . LexToken(LET,'let',1,12)
Action : Shift and goto state 27

State  : 27
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET . LexToken(ident,'x',1,16)
Action : Shift and goto state 42

State  : 42
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident . LexToken(ASSIGNMENT_OP,'=',1,18)
Action : Shift and goto state 56

State  : 56
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP . LexToken(LIT_INTEGER,5,1,20)
Action : Shift and goto state 95

State  : 95
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER . LexToken(SEMICOLON,';',1,21)
Action : Shift and goto state 118

State  : 118
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON . LexToken(LET,'let',1,24)
Action : Reduce rule [non_block_stmt -> LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON] with ['let','x','=',5,';'] and goto state 9
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt . LexToken(LET,'let',1,24)
Action : Shift and goto state 27

State  : 27
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET . LexToken(MUT,'mut',1,28)
Action : Shift and goto state 41

State  : 41
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET MUT . LexToken(ident,'done',1,32)
Action : Shift and goto state 55

State  : 55
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET MUT ident . LexToken(ASSIGNMENT_OP,'=',1,37)
Action : Shift and goto state 91

State  : 91
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET MUT ident ASSIGNMENT_OP . LexToken(TRUE,'true',1,39)
Action : Shift and goto state 114

State  : 114
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET MUT ident ASSIGNMENT_OP TRUE . LexToken(SEMICOLON,';',1,43)
Action : Shift and goto state 131

State  : 131
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt LET MUT ident ASSIGNMENT_OP TRUE SEMICOLON . LexToken(LOOP,'loop',1,47)
Action : Reduce rule [non_block_stmt -> LET MUT ident ASSIGNMENT_OP TRUE SEMICOLON] with ['let','mut','done','=','true',';'] and goto state 15
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt . LexToken(LOOP,'loop',1,47)
Action : Shift and goto state 28

State  : 28
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP . LexToken(BEGIN_BLOCK,'{',1,52)
Action : Shift and goto state 43

State  : 43
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK . LexToken(LOOP,'loop',1,56)
Action : Shift and goto state 28

State  : 28
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP . LexToken(BEGIN_BLOCK,'{',1,61)
Action : Shift and goto state 43

State  : 43
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK . LexToken(LET,'let',1,66)
Action : Shift and goto state 27

State  : 27
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK LET . LexToken(ident,'y',1,70)
Action : Shift and goto state 42

State  : 42
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK LET ident . LexToken(ASSIGNMENT_OP,'=',1,72)
Action : Shift and goto state 56

State  : 56
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK LET ident ASSIGNMENT_OP . LexToken(LIT_INTEGER,4,1,74)
Action : Shift and goto state 95

State  : 95
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER . LexToken(SEMICOLON,';',1,75)
Action : Shift and goto state 118

State  : 118
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON . LexToken(PRINT,'println!',1,80)
Action : Reduce rule [non_block_stmt -> LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON] with ['let','y','=',4,';'] and goto state 9
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt . LexToken(PRINT,'println!',1,80)
Action : Shift and goto state 12

State  : 12
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt PRINT . LexToken(OPEN_PARANTHESIS,'(',1,88)
Action : Shift and goto state 33

State  : 33
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt PRINT OPEN_PARANTHESIS . LexToken(SENTENCE,'"Hello, World"',1,89)
Action : Shift and goto state 49

State  : 49
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt PRINT OPEN_PARANTHESIS SENTENCE . LexToken(CLOSE_PARANTHESIS,')',1,103)
Action : Shift and goto state 64

State  : 64
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS . LexToken(SEMICOLON,';',1,104)
Action : Shift and goto state 103

State  : 103
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON . LexToken(END_BLOCK,'}',1,108)
Action : Reduce rule [non_block_stmt -> PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON] with ['println!','(',<str @ 0xb723b598>,')',';'] and goto state 17
Result : <NoneType @ 0x833a224> (None)

State  : 25
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt non_block_stmt . LexToken(END_BLOCK,'}',1,108)
Action : Reduce rule [stmts -> non_block_stmt] with [None] and goto state 4
Result : <NoneType @ 0x833a224> (None)

State  : 40
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK non_block_stmt stmts . LexToken(END_BLOCK,'}',1,108)
Action : Reduce rule [stmts -> non_block_stmt stmts] with [None,None] and goto state 5
Result : <NoneType @ 0x833a224> (None)

State  : 57
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,108)
Action : Shift and goto state 97

State  : 97
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK LOOP BEGIN_BLOCK stmts END_BLOCK . LexToken(END_BLOCK,'}',1,112)
Action : Reduce rule [loop_block -> LOOP BEGIN_BLOCK stmts END_BLOCK] with ['loop','{',None,'}'] and goto state 42
Result : <NoneType @ 0x833a224> (None)

State  : 20
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK loop_block . LexToken(END_BLOCK,'}',1,112)
Action : Reduce rule [block_stmt -> loop_block] with [None] and goto state 35
Result : <NoneType @ 0x833a224> (None)

State  : 17
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK block_stmt . LexToken(END_BLOCK,'}',1,112)
Action : Reduce rule [stmts -> block_stmt] with [None] and goto state 6
Result : <NoneType @ 0x833a224> (None)

State  : 57
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,112)
Action : Shift and goto state 97

State  : 97
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt LOOP BEGIN_BLOCK stmts END_BLOCK . LexToken(END_BLOCK,'}',1,114)
Action : Reduce rule [loop_block -> LOOP BEGIN_BLOCK stmts END_BLOCK] with ['loop','{',None,'}'] and goto state 42
Result : <NoneType @ 0x833a224> (None)

State  : 20
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt loop_block . LexToken(END_BLOCK,'}',1,114)
Action : Reduce rule [block_stmt -> loop_block] with [None] and goto state 35
Result : <NoneType @ 0x833a224> (None)

State  : 17
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt block_stmt . LexToken(END_BLOCK,'}',1,114)
Action : Reduce rule [stmts -> block_stmt] with [None] and goto state 6
Result : <NoneType @ 0x833a224> (None)

State  : 40
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt non_block_stmt stmts . LexToken(END_BLOCK,'}',1,114)
Action : Reduce rule [stmts -> non_block_stmt stmts] with [None,None] and goto state 5
Result : <NoneType @ 0x833a224> (None)

State  : 40
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK non_block_stmt stmts . LexToken(END_BLOCK,'}',1,114)
Action : Reduce rule [stmts -> non_block_stmt stmts] with [None,None] and goto state 5
Result : <NoneType @ 0x833a224> (None)

State  : 14
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts . LexToken(END_BLOCK,'}',1,114)
Action : Shift and goto state 34

State  : 34
Stack  : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK . $end
Action : Reduce rule [main_fun -> FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK] with ['fn','main','(',')','{',None,'}'] and goto state 3
Result : <NoneType @ 0x833a224> (None)

State  : 2
Stack  : main_fun . $end
Action : Reduce rule [ProgramFile -> main_fun] with [None] and goto state 2
Result : <NoneType @ 0x833a224> (None)

State  : 4
Stack  : ProgramFile . $end
Action : Reduce rule [compilation_unit -> ProgramFile] with [None] and goto state 1
Result : <NoneType @ 0x833a224> (None)

State  : 1
Stack  : compilation_unit . $end
Done   : Returning <NoneType @ 0x833a224> (None)
PLY: PARSE DEBUG END
