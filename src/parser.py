import ply.yacc as yacc, sys
import subprocess
from lexer import tokens, lexer

lis = []
lis2 =[]

def p_compilation_unitt(p):
	''' compilation_unit : ProgramFile '''
	lis.append(p.slice)

def p_ProgramFile(p):
	''' ProgramFile : main_fun''' 
	lis.append(p.slice)

def p_main_fun(p):
	''' main_fun : FN MAIN_FUN OPEN_PARANTHESIS CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_stmts(p):
	''' stmts : non_block_stmt
		| non_block_stmt stmts
		| block_stmt 
		| block_stmt stmts '''
	lis.append(p.slice)

def p_non_block_stmt(p):
	''' non_block_stmt : LET ident ASSIGNMENT_OP SENTENCE SEMICOLON
		| LET ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON
		| LET ident ASSIGNMENT_OP LIT_FLOAT SEMICOLON
		| LET ident ASSIGNMENT_OP TRUE SEMICOLON
		| LET ident ASSIGNMENT_OP FALSE SEMICOLON
		| LET MUT ident ASSIGNMENT_OP LIT_INTEGER SEMICOLON
		| LET MUT ident ASSIGNMENT_OP LIT_FLOAT SEMICOLON
		| LET MUT ident ASSIGNMENT_OP TRUE SEMICOLON
		| LET MUT ident ASSIGNMENT_OP FALSE SEMICOLON
		| PRINT OPEN_PARANTHESIS SENTENCE CLOSE_PARANTHESIS SEMICOLON 
		| binop
		| bitop '''
	lis.append(p.slice)

def p_binop(p):
	''' binop : ident ASSIGNMENT_OP ident arith_op ident SEMICOLON '''
	lis.append(p.slice)

def p_binop_ex(p):
	''' binop_ex : arith_op ident
		| arith_op ident binop_ex '''
	lis.append(p.slice)

def p_arith_op(p):
	''' arith_op : PLUS
		| MINUS
		| MULTIPLICATION
		| DIVISION
		| MODULUS '''
	lis.append(p.slice)

def p_bitop(p):
	''' bitop : ident ASSIGNMENT_OP ident bit_op ident SEMICOLON '''
	lis.append(p.slice)

def p_bit_op(p):
	''' bit_op : AND_LOGICAL
		| OR_LOGICAL
		| XOR_OP
		| NOT_OP
		| LEFT_SHIFT
		| RIGHT_SHIFT '''
	lis.append(p.slice)

def p_block_stmt(p):
	''' block_stmt : loop_block
		| while_block
		| for_block 
		| if_block 
		| if_else_block
		| else_if_block
		| unsafe_block '''
	lis.append(p.slice)

def p_loop_block(p):
	''' loop_block : LOOP BEGIN_BLOCK stmts END_BLOCK'''	
	lis.append(p.slice)

def p_while_block(p):
	''' while_block : WHILE TRUE BEGIN_BLOCK stmts END_BLOCK 
		| WHILE FALSE BEGIN_BLOCK stmts END_BLOCK 
		| WHILE ident BEGIN_BLOCK stmts END_BLOCK
		| WHILE ident EQUALS_COMP LIT_INTEGER BEGIN_BLOCK stmts END_BLOCK 
		| WHILE ident EQUALS_COMP LIT_FLOAT BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_for_block(p):
	''' for_block : FOR ident IN LIT_INTEGER D_DOT LIT_INTEGER BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_if_block(p):
	''' if_block : IF OPEN_PARANTHESIS ident operator LIT_INTEGER CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK
		| IF OPEN_PARANTHESIS ident operator EQUALS_COMP LIT_FLOAT CLOSE_PARANTHESIS BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_operator(p):
	''' operator : EQUALS_COMP
		| GREATER_EQUAL_OP
		| LESS_EQUAL_OP
		| LESS_THAN
		| GREATER_THAN
		| LESSEQUAL
		| NOTEQUAL
		| OR_OP
		| AND_OP '''
	lis.append(p.slice)

def p_if_else_block(p):
	''' if_else_block : if_block ELSE BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_else_if_block(p):
	''' else_if_block : if_block ELSE if_block ELSE BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_unsafe_block(p):
	''' unsafe_block : UNSAFE BEGIN_BLOCK stmts END_BLOCK '''
	lis.append(p.slice)

def p_error(p):
	print "ERROR: error in parsing phase "+ str(p)
	lis.append(p.slice)

parser = yacc.yacc(start='compilation_unit')
#lis.reverse()


	

# Read the input program
filename = sys.argv[1]
inputfile = open(filename, 'r')
data = inputfile.read()
result = parser.parse(data, lexer=lexer, debug=2)
print "<table>"
for i in range(1,len(lis)):
	print "<tr>"
	for j in range(1,len(lis[i])):
			print "<td>"
			print lis[i][j]
			print "</td>\n"
	print "</tr>"
print "</table>" 
			

